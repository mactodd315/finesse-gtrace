Installation
===============================

To install, simply clone the `repository <https://gitlab.com/mactodd315/fintrace>`_ to a desirable path:

.. code-block:: bash

    cd /path/to/desired/directory/for/repos
    git clone git@gitlab.com:mactodd315/fintrace.git

Then ``cd`` into the ``fintrace`` directory and install it. Note, if
you have your own environment you'd like to install it in, you must
have that active; otherwise follow the directions for creating the 
``fintrace`` environment.

.. code-block:: bash

    cd fintrace
    conda env create -f environment.yml # only if you want fintrace env!
    pip install -e .

You will also need to download and install the ``cosmic-explorer`` repository
which contains several component objects that are not native to ``finesse``, 
thus must be installed explicitly.

.. code-block:: bash

    conda activate fintrace # only if you want fintrace env! otherwise make sure you have your own env activated
    cd /path/to/desired/directory/for/repos
    git clone https://gitlab.com/cosmic-explorer/cosmic-explorer.git

Then ``cd`` into the ``cosmic-explorer`` directory and install it with:

.. code-block:: bash

    cd cosmic-explorer
    pip install -e .

Then, to test that everything has installed correctly:

.. code-block:: bash

    cd /path/to/fintrace/repo    
    fintrace_plot_yaml examples/bowtie/bowtie_test.yaml \
    --gtrace examples/bowtie/figures/bowtie_cavity \
    --beam-origin cavity  \
    --draw-width --grid  \
    --cavity-trace all \
    --beam-trace M3.fr2.o M4.fr1.i \
    --q-in 0+4j \
    --save-path examples/bowtie/figures/ \
    --verbose

You should see the new figures directory in the examples folder, containing each of the plots created by the
script.