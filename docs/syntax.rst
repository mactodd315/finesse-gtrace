===============================
Syntax for yamlfiles
===============================

Each of the following subsections represent a possible **Type** keyword
for the yamlfile. In each of them is described what arguments can be passed
as keywords to that object. Boldfaced arguments are required, however ``Connections``
are special in that all possible connections of a part are required but you can
leave it blank if such a connection is not made in the model. For connections,
all that is needed is the optic name (e.g. M1 Front: M2), the corresponding
optic should also reflect that it is connected to the first 
(e.g. M2 Front: M1).

-------------------------------
Mirrors
-------------------------------
Mirrors should be used in the model when you know an optic 
will have a zero angle of incidence with the beam. Front is
considered the HR side and Back is the AR side. The object
header (the first key containing all of these) should be the 
name of the optic. Note, the position of all optics is considered
to be the center of the HR face.

**Required Keys:**

* **Type: Mirror**
* **Position**

    * **X** : (in meters)
    * **Y** : (in meters)

* **Connections**

    * **Front** : *(can leave blank)*
    * **Back**  : *(can leave blank)*

Optional Keys:

* T : *transmission, default = .1*
* L : *loss (note R = 1 - T - L), default = 0*
* R_AR : *reflection of AR, default = 0*
* thickness : *substrate thickness, default = 5mm*
* nr : *index of refraction of substrate, default = 1.45*
* phi : *phase offset of mirror, only influences finesse model, default = 0*
* Rc : *radius of curvature, default = Inf*
* Rc_AR : *radius of curvature of AR, default = Inf*
* diameter : *diameter of mirror face, default = 5cm*
* wedgeAngle : *wedge-angle relative to HR surface, default = 0 deg*
* normAngle : *pointing angle of HR surface, default = calculated by connections*

-------------------------------
Beamsplitters
-------------------------------

Beamsplitters should be used in the model when you know an optic 
will have an angle of incidence with the beam, even if physically
that optic is just a mirror (e.g. a steering mirror can be 
considered a beampslitter in this instance). Front1 and Front2 are
considered the HR side and Back1 and Back2 are the AR side. 
The object header (the first key containing all of these) should be the 
name of the optic.

**Required Keys:**

* **Type: Beamsplitter**
* **Position**

    * **X** : (in meters)
    * **Y** : (in meters)

* **Connections**

    * **Front1** : *(can leave blank)*
    * **Front2** : *(can leave blank)*
    * **Back1**  : *(can leave blank)*
    * **Back2**  : *(can leave blank)*

Optional Keys:

* T : *transmission, default = .1*
* L : *loss (note R = 1 - T - L), default = 0*
* R_AR : *reflection of AR, default = 0*
* thickness : *substrate thickness, default = 5mm*
* nr : *index of refraction of substrate, default = 1.45*
* phi : *phase offset of mirror, only influences finesse model, default = 0*
* Rc : *radius of curvature, default = Inf*
* Rc_AR : *radius of curvature of AR, default = Inf*
* diameter : *diameter of mirror face, default = 5cm*
* wedgeAngle : *wedge-angle relative to HR surface, default = 0 deg*
* alpha : *angle-of-incidence calculated by connection graph*
* normAngle : *pointing angle of HR surface, default = calculated by connections*

-------------------------------
Lens
-------------------------------
Lens should be used in the model when you know an optic 
will have a zero angle of incidence with the beam. The object
header (the first key containing all of these) should be the 
name of the optic. Note, the position of all optics is considered
to be the center of the HR(front) face.

**Required Keys:**

* **Type: Lens**
* **Position**

    * **X** : (in meters)
    * **Y** : (in meters)

* **Connections**

    * **Front** : *(can leave blank)*
    * **Back**  : *(can leave blank)*

Optional Keys:

* Focal Length : *focal length of lens, will add a Thin Lens to the model*
* Rc : *Radius of curvature of front and back faces for a thick lens(note must declare with front and back), default = inf*

    * Front :
    * Back :

* thickness : *substrate thickness, default = 5mm*
* nr : *index of refraction of substrate, default = 1.45*
* phi : *phase offset of mirror, only influences finesse model, default = 0*
* diameter : *diameter of mirror face, default = 5cm*
* normAngle : *pointing angle of HR surface, default = calculated by connections*

-------------------------------
Lasers
-------------------------------
Lasers should be used in the model as a source to the beam for both
the finesse model and gtrace rendering. The default is a collimated 3mm beam 
pointed 0 degrees from the right horizontal direction. The user can specify the 
power in the beam, as well as the beam parameters. This can be done be either
specifying the q-parameter explicitly or the RoC and beamwidth at the laser head.
The user can also decide to let the model calculate the beam-parameter based on 
the designated cavity mode-matching requirements.

**Required Keys:**

* **Type: Laser**
* **Position**

    * **X** : (in meters)
    * **Y** : (in meters)

* **Connections**

    * **Front** : *(can leave blank, but shouldn't)*

Optional Keys:

* P : *beam power, default = 1 [W]*
* q0 : *beam-parameter, default = 26.573599624060147j*
* RoC: *radius of curvature of beam, default = Inf*
* w : *beamwidth, default = 3mm*
* Wavelength : *beam wavelength, default = 1064nm*
* Match: *name of cavity to mode match to, will overwrite any defined q or RoC/w.*
* normAngle : *pointing angle of HR surface, default = calculated by connections*

-------------------------------
Cavities
-------------------------------

Cavities are defined in ``Finesse`` style, in that all you 
need is a starting node, and can define an optional `via` node,
(e.g. FabryPerot, From: M1.fr.o, Via: M2.fr.i).

**Required Keys:**

* **Type: Cavity**
* **From:**

Optional Keys:

* Via: 

-------------------------------
Photodiodes
-------------------------------

Photodiodes are defined by declaring a port that is being 
measured. Should the user wish to implement a demodulated 
photodiode, just declare it as an optional key.

**Required Keys:**

* **Type: Photodiode**
* **Position:**

    * **X** : (in meters)
    * **Y** : (in meters)

* **Port:** : (uses finesse sytnax for which port/node to connect to)

Optional Keys:

* Omega : frequency to demodulate signal [Hz]
* Phase : phase to rotate I,Q basis [deg]
* width : width of Photodiode face for gtrace rendering [m]

-------------------------------
Grids
-------------------------------

Grids are only for gtrace renderings and do not add anything to the
finesse models. You do not have to define the grid to add 
one to the gtrace drawing, declaring ``--grid True`` without
a ``Grid`` in the yamlfile, one will be calculated from 
the dynamic range from the drawing.

**Required Keys:**

* **Type: Grid**
* **Xlim:** : (list of [min and max], in meters)
* **Ylim:** : (list of [min and max], in meters)

Optional Keys:

* Xstep : step between xmin and xmax, dictating the number of vertical gridlines
* Ystep : step between ymin and ymax, dictating the number of horizontal gridlines

-------------------------------
Circles
-------------------------------

Circles are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Circle**
* **Center:** : 

    * **X** : (in meters)
    * **Y** : (in meters)

* **Radius** : (in meters)

Optional Keys:

* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)

-------------------------------
Rectangles
-------------------------------

Rectangles are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Rectangle**
* **Center:** : 

    * **X** : (in meters)
    * **Y** : (in meters)

* **Height** : (in meters)
* **Width** : (in meters)

Optional Keys:

* Thickness : default is .01.
* normAngle: in degrees, default is 0.
* Color : (R, G, B), default is (255, 255, 255)

-------------------------------
Lines
-------------------------------

Lines are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Line**
* **Points:** : 

    * **X** : (list of 2 x points, in meters)
    * **Y** : (list of 2 y points, in meters)

Optional Keys:

* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)

-------------------------------
PolyLines
-------------------------------

PolyLines are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: PolyLine**
* **Points:** : 

    * **X** : (list of many x points, in meters)
    * **Y** : (list of many y points, in meters)

Optional Keys:

* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)

-------------------------------
Arcs
-------------------------------

Arcs are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Arc**
* **Center:** : 

    * **X** : (in meters)
    * **Y** : (in meters)

* **Radius** : (in meters)
* **StartAngle** : (in degrees)
* **StopAngle** : (in degrees)

Optional Keys:

* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)

-------------------------------
Texts
-------------------------------

Texts are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Text**
* **Text** :
* **Center:** : 

    * **X** : (in meters)
    * **Y** : (in meters)

* **Height** : (in meters)
* **Rotation** : (in degrees)

Optional Keys:

* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)


-------------------------------
Hexagons
-------------------------------

Hexagons are only for gtrace renderings and do not add anything
to finesse models.

**Required Keys:**

* **Type: Hexagon**
* **Center:** : 

    * **X** : (in meters)
    * **Y** : (in meters)

* **Radius** : (in meters)

Optional Keys:

* normAngle : [deg] defualt is 0 
* Thickness : default is .01.
* Color : (R, G, B), default is (255, 255, 255)