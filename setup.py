"""Fintrace setup.
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib, os

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

def find_files(dirname, relpath=None):
    def find_paths(dirname):
        items = []
        for fname in os.listdir(dirname):
            path = os.path.join(dirname, fname)
            if os.path.isdir(path):
                items += find_paths(path)
            elif not path.endswith(".py") and not path.endswith(".pyc"):
                items.append(path)
        return items
    items = find_paths(dirname)
    if relpath is None:
        relpath = dirname
    return [os.path.relpath(path, relpath) for path in items]

setup(
    name="fintrace",
    version="1.0.0a1", 
    description="An interferometer modeling and ray tracing program", 
    long_description=long_description, 
    long_description_content_type="text/markdown",  
    url="https://gitlab.com/mactodd315/fintrace",  
    author="Matthew Todd",  
    author_email="mrtodd@syr.edu",  
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Physics",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3 ",
    ],
    # This field adds keywords for your project which will appear on the
    # project page. What does your project relate to?
    #
    # Note that this is a list of additional keywords, separated
    # by commas, to be used to assist searching for the distribution in a
    # larger catalog.
    keywords="optics, ifo-modeling, ray-tracing",  # Optional
    # When your source code is in a subdirectory under the project root, e.g.
    # `src/`, it is necessary to specify the `package_dir` argument.
    package_dir={"": "src",},  
    packages=find_packages(where="src"),  
    python_requires=">=3.8, <4",

    install_requires=["finesse", "gtrace", 'prettytable','termcolor',
                      'ezdxf'],  # Optional
    scripts=['bin/fintrace_plot_yaml'],
    project_urls={  # Optional
        "Bug Reports": "https://gitlab.com/mactodd315/fintrace/-/issues",
    },
)