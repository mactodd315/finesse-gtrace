.. fintrace documentation master file, created by
   sphinx-quickstart on Wed Jun 19 17:00:25 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fintrace's documentation!
====================================

Combining the interferometric modeling capabilities of `Finesse` with the 
layout modeling of `gtrace` is of interest to many in the optics and
physics community. With fintrace, by providing a yamlfile of the 
desired model, one can seamlessly utilize the functionality of both 
`Finesse` and `gtrace`, in both the command-line and scripts.

Links to the respective packages documentations can be found here:
`finesse <https://finesse.ifosim.org/docs/latest/index.html>`_
`gtrace <https://gtrace.readthedocs.io/en/latest/index.html>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   syntax
   tutorial
